import React, { Component } from "react";
import Header from "./component/Header";
import FetchApi from "./component/FetchAPI";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import SelectData from "./component/Selectdata/SelectData";
import AddProduct from "./component/AddProduct/AddProduct";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      userAddedProducts: [],
    };
  }

  AddProductdata = (data) => {
    const { userAddedProducts } = this.state;

    const newProduct = {
      title: data.title,
      category: data.category,
      price: data.price,
      description: data.description,
      rating: {
        rate: data.rating,
        rate: data.count,
      },
      image: data.image
    };

    this.setState({
      userAddedProducts: [...userAddedProducts, newProduct],
    });
  };

  onSearchChange = (event) => {
    this.setState({
      search: event.target.value,
    });
  };
  render() {
    return (
      <>
        <BrowserRouter>
          <Header search={this.onSearchChange} />
          <Routes>
            <Route
              path="/"
              element={
                <FetchApi
                  userAddedProducts={this.state.userAddedProducts}
                  searchFor={this.state.search}
                />
              }
            />
            <Route path="/:id" element={<SelectData />} />
            <Route
              path="/addProductForm"
              element={<AddProduct addProduct={this.AddProductdata} />}
            />
          </Routes>
        </BrowserRouter>
      </>
    );
  }
}
