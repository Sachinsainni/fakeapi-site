import React, { Component } from "react";
// import { Link } from "react-router-dom";
import AddProductInput from "./AddProductInput";
import validator from "validator";
import "./AddProduct.css";

export default class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      image: "",
      category: "",
      price: "",
      description: "",
      rating: "",
      titleError: "",
      imageError: "",
      categoryError: "",
      priceError: "",
      descriptionError: "",
      ratingError: "",
    };
  }

  onInput = (event) => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  };

  onSubmit = () => {
    let flag = true;
    if (!validator.isAlpha(this.state.title)) {
      this.setState({
        titleError: "Please Enter Valid Title",
      });
      flag = false;
    } else {
      this.setState({
        titleError: "",
      });
    }
    if (!validator.isURL(this.state.image)) {
      this.setState({
        imageError: "Please Enter Valid image URL",
      });
      flag = false;
    } else {
      this.setState({
        imageError: "",
      });
    }
    if (!validator.isAlpha(this.state.category)) {
      this.setState({
        categoryError: "Please Enter Valid Category",
      });
      flag = false;
    } else {
      this.setState({
        categoryError: "",
      });
    }
    if (!validator.isAlpha(this.state.description)) {
      this.setState({
        descriptionError: "Please Enter Valid Description",
      });
      flag = false;
    } else {
      this.setState({
        descriptionError: "",
      });
    }
    if (!validator.isNumeric(this.state.price)) {
      this.setState({
        priceError: "Please Enter Valid Price",
      });
      flag = false;
    } else {
      this.setState({
        priceError: "",
      });
    }
    if (!validator.isNumeric(this.state.rating)) {
      this.setState({
        ratingError: "Please Enter Valid Rating",
      });
      flag = false;
    } else {
      this.setState({
        ratingError: "",
      });
    }
    if (flag === true) {
      this.props.addProduct(this.state);
    }
  };

  render() {
    return (
      <>
        
        <div id="add-product">
          <AddProductInput
            title="Title"
            id="title"
            onInput={this.onInput}
            error={this.state.titleError}
          />
          <AddProductInput
            title="Image Url"
            id="image"
            onInput={this.onInput}
            error={this.state.imageError}
          />
          <AddProductInput
            title="Price"
            id="price"
            onInput={this.onInput}
            error={this.state.priceError}
          />
          <AddProductInput
            title="Category"
            id="category"
            onInput={this.onInput}
            error={this.state.categoryError}
          />
          <AddProductInput
            title="Description"
            id="description"
            onInput={this.onInput}
            error={this.state.descriptionError}
          />
          <AddProductInput
            title="Rating"
            id="rating"
            onInput={this.onInput}
            error={this.state.ratingError}
          />
          <div>
            <button onClick={this.onSubmit}>Add Product</button>
          </div>
        </div>
      </>
    );
  }
}
