import React from "react";

export default function AddProductInput(props) {
  return (
    <>
      <div className="add-product">
        <label htmlFor={props.id}>{props.title}</label>
        <input 
        type="text"
        id={props.id} 
        onChange={props.onInput} 
        />
        <span>
            {props.error === '' ? <>&nbsp;</> : props.error}
        </span>
      </div>
    </>
  );
}
