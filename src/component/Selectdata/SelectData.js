import Axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import "../Selectdata/SelectData.css";

export default function SelectData() {
  const appState = {
    LOADING: "loading",
    LOADED: "loaded",
    ERROR: "error",
  };

  const { id } = useParams();

  const [product, setProduct] = useState([]);
  const [isLoad, setIsLoad] = useState(appState.LOADING);

  useEffect(() => {
    Axios.get(`https://fakestoreapi.com/products/${id}`)
      .then((response) => {
        setIsLoad(appState.LOADED);
        return setProduct(response.data);
      })
      .catch((error) => {
        console.log(error);
        setIsLoad(appState.ERROR);
      });
  }, []);

  return (
    <div className="select-data-home">
      {isLoad === appState.LOADING ? (
        <div className="lds-hourglass">loading</div>
      ) : undefined}
      {isLoad === appState.ERROR ? (
        <div className="error-message">An error occured </div>
      ) : undefined}
      {isLoad === appState.LOADED ? (
        <>
          <div id="select-data">
            <h1 className="title">{product.title}</h1>
            <div className="details">
              <div className="image-data">
              <img src={product.image} alt="" />
              </div>
             <div className="data-detail">
             <p className="description">{product.description}</p>
              <p className="category">Catagory : {product.category}</p>
              <p className="price">Price : &#x24;{product.price}</p>
              <p className="rate">Rating : {product.rating.rate}&#9733;</p>
            <button type="submit">Click to Buy</button>
             </div>
            </div>
          </div>
        </>
      ) : undefined}
    </div>
  );
}
