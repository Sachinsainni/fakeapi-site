import React from "react";
import { Link } from "react-router-dom";

export default function Details(props) {
  const data = props.dataDetail;

  return (
    <>
      <Link to={`/${data.id}`} className="dataDetails">
      
            <div className="data-data" key={data.id}>
              <p className="title">{data.title}</p>
              <img src={data.image} alt="" />
              <p className="category">Catagory : {data.category}</p>
              <p className="price">Price : &#x24;{data.price}</p>
              <p className="description">{data.description}</p>
              <p className="rate">Rating : {data.rating.rate}&#9733;</p>
            </div>
          
      </Link>
    </>
  );
}
