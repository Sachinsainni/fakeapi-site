import React from "react";
import { Link } from "react-router-dom";

export default function Header(props) {
  return (
    <div id="header">
      <div id="logo">
        <h1>ShopHere</h1>
      </div>
      <div id="search-bar">
        <input
          type="search"
          id="search-input"
          onChange={props.search}
          placeholder="Search Anything"
        />
      </div>
      <Link to="/">
          <button>View All Products</button>
        </Link>
      <Link to='/addProductForm'>
        <button >Add Product</button>
      </Link>
    </div>
  );
}
