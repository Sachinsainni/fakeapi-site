import React, { Component } from "react";
import Axios from "axios";
import Details from "./Details";

export default class FetchAPI extends Component {
 
  constructor(props) {
    super(props);

    this.appState = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      status: this.appState.LOADING,
      data: [],
    };
  }
  getData = () => {
   
    Axios.get(`https://fakestoreapi.com/products`)
      .then((response) => {
         const {userAddedProducts} = this.props
        this.setState({
          data: [...response.data, ...userAddedProducts],
          status: this.appState.LOADED,
        });
      },()=>{
        console.log(this.state)
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          status: this.appState.ERROR,
        });
      });
  };

  componentDidMount() {
    this.getData();
  }

  render() {
    let allData = this.state.data;
    const searchProduct = allData.filter((each) => {
      return each.title
        .toLowerCase()
        .includes(this.props.searchFor.toLowerCase());
    });
    return (
      <>
        <div className="data-container">
          {this.state.status === this.appState.LOADING ? (
            <div className="lds-hourglass">loading</div>
          ) : undefined}
          {this.state.status === this.appState.ERROR ? (
            <div className="error-message">An error occured </div>
          ) : undefined}
          {this.state.status === this.appState.LOADED
            ? searchProduct.map((each, id) => {
                return <Details key={id} dataDetail={each} />;
              })
            : undefined}
          {this.state.status !== this.appState.LOADING &&
          searchProduct.length === "" ? (
            "No Product Available"
          ) : undefined}
        </div>
      </>
    );
  }
}
